﻿namespace Geekbrains.Enum
{
	public enum MouseButton : byte
	{
		LeftButton,
		RightButton
	}
}