﻿using Geekbrains;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : BaseObjectScene, ISetDamage
{
    public Transform goal;
    [SerializeField] private UnitPath _path;
    [SerializeField] float _hp = 100;
    [SerializeField] private float _seeDist = 10f;
    [SerializeField] private LayerMask _layerMask;
    [SerializeField] private SpriteRenderer _sprite;
    private UnityEngine.AI.NavMeshAgent agent;
    private Transform _target;
    private EnemyController _enemyController;
    private IMotor _player;

    public void SetDamage(InfoCollision info)
    {
        _hp -= info.Damage;
        if (_hp < 0)
        {
            _enemyController.Unregister(this);
            Destroy(gameObject);
        }
    }

    private void Awake()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        _sprite.enabled = false;
    }

    private void Start()
    {
        _enemyController = Messenger.GetController<EnemyController>();
        _enemyController.Register(this);
        _player = Messenger.GetController<PlayerController>().Unit;
    }

    public void Init()
    {
        _target = _path.GetNearestPoint(transform.position);
        agent.destination = _target.position;
    }

    private bool _follow;

    bool CanSeePlayer()
    {
        var tmp = _player.Position - transform.position;
        return Vector3.Dot(transform.forward, tmp) > 0 &&
               Vector3.Distance(transform.position, _player.Position) < _seeDist &&
               !Physics.Linecast(transform.position, _player.Position, _layerMask);
    }

    public void MyUpdate()
    {
        if (CanSeePlayer())
        {
            agent.destination = _player.Position;
            if (!_follow)
            {
                _follow = true;
                _sprite.enabled = true;
            }
        }
        else
        {
            if (_follow)
            {
                _target = _path.GetNearestPoint(transform.position);
                agent.destination = _target.position;
                _follow = false;
                _sprite.enabled = false;
            }
            else
            {
                if (agent.remainingDistance < 0.1f)
                {
                    _target = _path.GetNextPoint(_target);
                    agent.destination = _target.position;
                }
            }
        }
    }
}