﻿using Geekbrains;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BombController : BaseController
{
    bool loaded = false;
    public override void MyUpdate()
    {
        if (loaded) return;
        loaded = true;
        Awake();
    }
    void Awake()
    {
        //var str = JsonUtility.ToJson(new BombDataArray() {
        //    data = new BombData[] {
        //        new BombData { Position = Vector3.zero, Power = 1 }
        //    }
        //});
        //File.WriteAllText(Path.Combine(Application.dataPath, "BombData.json"), str);
        var bombPrefab = Resources.Load<GameObject>("Bomb");
        var json = File.ReadAllText(Path.Combine(Application.dataPath, "BombData.json"));
        var bombdata = JsonUtility.FromJson<BombDataArray>(json);
        for(int i = 0; i < bombdata.data.Length; i++)
        {
            var data = bombdata.data[i];
            var go = GameObject.Instantiate(bombPrefab, data.Position, Quaternion.identity);
            go.GetComponent<Bomb>().Damage = data.Power;
        }
    }
}
