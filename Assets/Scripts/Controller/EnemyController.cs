using System.Collections.Generic;

namespace Geekbrains
{
    public class EnemyController : BaseController
    {
        List<EnemyAI> _enemyAis = new List<EnemyAI>();

        public EnemyController()
        {
            Messenger.Register(this);
        }

        public void Register(EnemyAI ai)
        {
            if(_enemyAis.Contains(ai))
                return;
            _enemyAis.Add(ai);
            ai.Init();
        }

        public void Unregister(EnemyAI ai)
        {
            if (_enemyAis.Contains(ai))
                _enemyAis.Remove(ai);
        }
        
        public override void MyUpdate()
        {
            foreach (var ai in _enemyAis)
            {
                ai.MyUpdate();
            }
        }
        
        
    }
}