﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geekbrains
{
    public  class WeaponController : BaseController
    {
        [SerializeField] WeaponUI _weaponUI;
        private Weapon[] _weapons;
        int pos = -1;
        public WeaponController()
        { 
            Messenger.Register(this);
            _weapons = GameObject.FindGameObjectWithTag("Player").transform.GetComponentsInChildren<Weapon>();
            foreach(var weapon in _weapons)
            {
                weapon.gameObject.SetActive(false);
            }
            NextWeapon();
            _weaponUI = Messenger.Get<WeaponUI>();
            UpdateAmmo();
        }
        [SerializeField] Weapon _weapon;
        public override void MyUpdate()
        {
            if (Input.GetButtonDown("Fire1")) 
            {
                _weapon.Fire(alt:false);
                UpdateAmmo();
            }
            if (Input.GetButtonDown("Fire2"))
            {
                _weapon.Fire(alt: true);
                UpdateAmmo();
            }
            if (Input.GetButtonDown("Reload"))
            {
                _weapon.Reload();
            }
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                NextWeapon();
                UpdateAmmo();
            }
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                PrevWeapon();
                UpdateAmmo();
            }
        }
        public void UpdateAmmo()
        {
            _weaponUI.ChangeTextLabel(_weapon.CurrentAmmo, _weapon.MaxAmmo);

        }
        public void PrevWeapon()
        {
            pos--;
            if (pos < 0)
                pos = _weapons.Length - 1;
            EquipWeapon(pos);
        }
        public void NextWeapon()
        {
            
            pos++;
            if (pos >= _weapons.Length)
                pos = 0;
            EquipWeapon(pos);
        }
        void EquipWeapon()
        {
            EquipWeapon(pos);
        }
        public void EquipWeapon(int pos)
        {
            _weapon?.gameObject.SetActive(false);
            _weapon = _weapons[pos];
            _weapon?.gameObject.SetActive(true);
            _weapon?.Equip();
        }
        public override void Off()
        {
            base.Off();
        }

        public override void On()
        {
            base.On();
        }

        public override void On(BaseObjectScene obj = null)
        {
            base.On(obj);
        }
    }
}