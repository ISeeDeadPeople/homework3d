﻿using UnityEngine;

namespace Geekbrains
{
    public sealed class FlashLightController : BaseController
    {
        private FlashLight _flashLight;
        private FlashLightUI _flashLightUI;

        public FlashLightController()
        {
            _flashLight = MonoBehaviour.FindObjectOfType<FlashLight>();
            _flashLight.Switch(false);
            _flashLightUI = MonoBehaviour.FindObjectOfType<FlashLightUI>();
            _flashLightUI.UpdateColor();
        }

        public override void MyUpdate()
        {
            if (!_flashLight) return;
            _flashLight.Rotation();

            if (_flashLight.EditBatteryCharge())
            {
                _flashLightUI.Text = _flashLight.BatteryChargePercent;
            }
            else
            {
                Off();
            }

        }

        public override void On()
        {
            if (IsActive) return;
            base.On();
            _flashLight.Switch(true);
            _flashLightUI.SetActive(true);
        }

        public override void Off()
        {
            if (!IsActive) return;
            base.Off();
            _flashLight.Switch(false);
            //_flashLightUI.SetActive(false);
        }
    }
}