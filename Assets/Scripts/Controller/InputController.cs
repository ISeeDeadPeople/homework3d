﻿using UnityEngine;

namespace Geekbrains
{
	public class InputController : BaseController
	{
		private KeyCode _activeFlashLight = KeyCode.F;
		public override void MyUpdate()
		{
			if(!IsActive) return;
			if (Input.GetKeyDown(_activeFlashLight))
			{
				Main.Instance.FlashLightController.Switch();
			}
		}
	}
}