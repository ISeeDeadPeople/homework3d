﻿using UnityEngine;

namespace Geekbrains
{
	public class PlayerController : BaseController
	{
		public IMotor Unit => _unit;

		private IMotor _unit;

		public PlayerController(IMotor player)
		{
			_unit = player;
			Messenger.Register(this);
		}

		public override void MyUpdate()
		{
			_unit.Move();
		}
	}
}