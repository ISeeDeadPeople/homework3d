﻿using UnityEngine;
using UnityEngine.UI;

namespace Geekbrains
{
    public class FlashLightUI : MonoBehaviour
    {
        [SerializeField] private Text _text;
        [SerializeField] private Image _image;
        [SerializeField] private Outline _outline;
        [SerializeField] private Gradient _gradient;

        private void Start()
        {
        }

        public float Text
        {
            set
            {
                _image.fillAmount = value;
                UpdateColor(value);
            }
        }
        public void UpdateColor()
        {
            UpdateColor(_image.fillAmount);
        }
        public void UpdateColor(float value)
        {
            var color = _gradient.Evaluate(_image.fillAmount);

            _text.text = (_image.fillAmount * 100f).ToString("0.0") + "%";
            _image.color = color;
            _text.color = color;
            _outline.effectColor = color;
        }
        public void SetActive(bool value) => _text.gameObject.SetActive(value);
    }
}