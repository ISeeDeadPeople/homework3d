﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponUI : MonoBehaviour {

    [SerializeField] public Color Color;
    [SerializeField] Text _ammoLabel;
    private void Awake()
    {
        Messenger.Register(this);

    }
    public void ChangeTextLabel(int currentAmmo, int maxAmmo)
    {
        _ammoLabel.text = $"{ currentAmmo} / {maxAmmo}";
    }
    
}
