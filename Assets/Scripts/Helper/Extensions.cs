﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public static class Extensions
{
    public static bool TryBool(this string self)
    {
        bool result;
        return Boolean.TryParse(self, out result) && result;
    }
}