﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class UnitPath : MonoBehaviour
{
    [SerializeField]private Transform[] _points;
    
    void Awake()
    {
        _points = transform.GetComponentsInChildren<Transform>(true);
        
    }

    public Transform GetNearestPoint(Vector3 position)
    {
        Transform toReturn = _points[1];
        float dist = Vector3.Distance(position, toReturn.position);
        for (int i = 1; i < _points.Length; i++)
        {
            var pointPos = _points[i].position;
            var distance = Vector3.Distance(pointPos, position);
            if (distance < dist)
            {
                toReturn = _points[i];
                dist = distance;
            }
        }

        return toReturn;
    }

    public Transform GetNextPoint(Transform point)
    {
        var pos = System.Array.IndexOf(_points, point);
        pos++;
        if (pos >= _points.Length)
            pos = 1;
        return _points[pos];
    }
    
}
