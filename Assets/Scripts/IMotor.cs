﻿using UnityEngine;

namespace Geekbrains
{
	public interface IMotor
	{
		void Move();

		Vector3 Position
		{
			get;
		}
	}
}