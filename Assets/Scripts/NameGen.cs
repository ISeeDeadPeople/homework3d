﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
public class NameGen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GetText());
        var objects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
        for(int i = 0; i < objects.Length; i++)
        {
          //  Destroy(objects[i]);
        }
    }

    IEnumerator GetText()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("https://www.seventhsanctum.com/generate.php?Genname=fantasynameex"))
        {
            yield return www.Send();
            List<string> names = new List<string>();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                MatchCollection matches = Regex.Matches(www.downloadHandler.text, "<div class=\"(GeneratorResultSecondaryBG|GeneratorResultPrimeBG)\">(?<userName>.*?)</div>");
                foreach( Match match in matches)
                {
                    var gr = match.Groups["userName"];
                    names.Add(gr.ToString());
                }
            }
        }
    }
}
