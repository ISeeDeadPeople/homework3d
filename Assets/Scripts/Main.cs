﻿using System.Collections.Generic;
using UnityEngine;

namespace Geekbrains
{
	public sealed class Main : MonoBehaviour
	{
		public InputController InputController { get; private set; }
		public FlashLightController FlashLightController { get; private set; }
		public PlayerController PlayerController { get; private set; }
		private List<BaseController> _controllers = new List<BaseController>();
        public WeaponController WeaponController;
		public EnemyController EnemyController;
		public static Main Instance { get; private set; }

		private void Awake()
		{
			
            Screen.lockCursor = true;
			Instance = this;

			PlayerController = new PlayerController(new UnitMotor(GameObject.FindGameObjectWithTag("Player").transform));
			_controllers.Add(PlayerController);

			InputController = new InputController();
			InputController.On();
			_controllers.Add(InputController);

			FlashLightController = new FlashLightController();
			_controllers.Add(FlashLightController);

            WeaponController = new WeaponController();
            _controllers.Add(WeaponController);
			
			EnemyController = new EnemyController();
			_controllers.Add(EnemyController);

            BombController bc = new BombController();
            _controllers.Add(bc);
		}

		private void Update()
		{
			if (Input.GetKeyDown("escape"))
				Screen.lockCursor = false;
			foreach (var controller in _controllers)
			{
				controller.MyUpdate();
			}
		}
	}
}