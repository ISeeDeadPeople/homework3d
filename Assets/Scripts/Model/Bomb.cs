using UnityEngine;

namespace Geekbrains
{
    public class Bomb : BaseObjectScene
    {
        [SerializeField] public float Damage;

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log(other.transform.name);
            var tempObj = other.gameObject.GetComponent<ISetDamage>();
            if (tempObj == null)
                return;
            tempObj.SetDamage(new InfoCollision(Damage, Vector3.up));
            Destroy(gameObject);
        }

    }
}