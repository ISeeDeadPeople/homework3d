﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct BombData
{
    [SerializeField]
    public int Power;
    [SerializeField]
    public Vector3 Position;
}
[System.Serializable]
public struct BombDataArray
{
    [SerializeField]
    public BombData[] data;
}