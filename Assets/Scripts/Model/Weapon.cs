﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Geekbrains
{
    public class Weapon : BaseObjectScene
    {
        [SerializeField] protected GameObject _barrel;
        [SerializeField] protected Bullet[] _bullets;
        [SerializeField] protected Animation _animation;
        [SerializeField] protected AnimationClip _reloadAdimation;
        [SerializeField] protected AnimationClip _initAdimation;
        protected bool _onReload = false;
        public int CurrentAmmo;
        public int MaxAmmo;


        public virtual void Fire(bool alt)
        {
            var bullet = _bullets[alt ? 1 : 0];
            if (CurrentAmmo - bullet.Size < 0) return;
            CurrentAmmo -= bullet.Size;
            var bulletObject = GameObject.Instantiate(bullet, _barrel.transform.position, Quaternion.identity);

            bulletObject.AddForce(_barrel.transform.forward * bulletObject.Force);
        }
        public virtual void Reload()
        {

            _onReload = true;
            _animation.clip = _reloadAdimation;
            _animation.Play();
        }
        public virtual void Equip()
        {
            _onReload = false;

            _animation.clip = _initAdimation;
            _animation.Play();

        }
        public virtual void UnEquip()
        {

        }
        public virtual void ReloadComplete()
        {
            CurrentAmmo = MaxAmmo;
            _onReload = false;
            Messenger.GetController<WeaponController>().UpdateAmmo();
        }

    }
}
