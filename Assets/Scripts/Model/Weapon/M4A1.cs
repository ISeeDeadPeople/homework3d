﻿using Geekbrains;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class M4A1 : Weapon
{

    public override void Fire(bool alt)
    {

        if (_onReload) return;
        base.Fire(alt);
        Debug.Log("Bang bang bang!!!");
    }

    public override void Equip()
    {
        base.Equip();
        
    }

    public override void Reload()
    {
        base.Reload();
        Debug.Log("Reload");

    }
}
