﻿using Geekbrains;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AK47 : Weapon
{
    public override void Fire(bool alt)
    {
        if (_onReload) return;
        base.Fire(alt);
    }

    public override void Equip()
    {
        base.Equip();

    }
    public override void UnEquip()
    {
        base.UnEquip();
    }
    public override void ReloadComplete()
    {
        base.ReloadComplete();

    }
    public override void Reload()
    {
        base.Reload();
        Debug.Log("Reload");

    }
}
