using UnityEngine;

namespace Geekbrains
{
    public class HealthPack : BaseObjectScene
    {
        [SerializeField] private float _healPower;
        private void OnCollisionEnter(Collision other)
        {
            var tempObj = other.gameObject.GetComponent<ISetDamage>();
            if (tempObj == null)
                return;
            tempObj.SetDamage(new InfoCollision(-_healPower, Vector3.up));
            Destroy(gameObject);
        }
    }
}