﻿using UnityEngine;

namespace Geekbrains
{
    public sealed class FlashLight : BaseObjectScene
    {
        private Light _light;
        private Transform _goFollow;
        private Vector3 _vecOffset;
        public float BatteryChargeCurrent { get; private set; }
        [SerializeField] private float _speed = 10;
        [SerializeField] private float _batteryChargeMax;
        public float BatteryChargePercent
        {
            get { return BatteryChargeCurrent / _batteryChargeMax; }
        }

        protected override void Awake()
        {
            base.Awake();
            _light = GetComponent<Light>();

            _goFollow = Camera.main.transform;
            _vecOffset = transform.position - _goFollow.position;
            BatteryChargeCurrent = _batteryChargeMax;
        }

        public void Switch(bool value)
        {
            _light.enabled = value;
            if (!value) return;
            transform.position = _goFollow.position + _vecOffset;
            transform.rotation = _goFollow.rotation;
        }

        public void Rotation()
        {
            if (!_light) return;

            transform.position = _goFollow.position + _vecOffset;
            transform.rotation = Quaternion.Lerp(transform.rotation, _goFollow.rotation, _speed * Time.deltaTime);
        }

        public bool EditBatteryCharge()
        {
            if(!_light.enabled && (int)BatteryChargePercent < 1)
            {
                BatteryChargeCurrent += Time.deltaTime;
                BatteryChargeCurrent = Mathf.Min(100, BatteryChargeCurrent);
                return true;
            }
            if (_light.enabled && BatteryChargePercent > 0)
            {
                BatteryChargeCurrent -= Time.deltaTime;
                BatteryChargeCurrent = Mathf.Max(BatteryChargeCurrent, 0);
                return true;
            }


            return false;
        }
    }
}