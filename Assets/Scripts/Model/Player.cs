﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
[Serializable]
public struct Player
{
    public string Name;
    public float Hp;
    public bool IsVisible;

    public override string ToString() => $"Name {Name} Hp {Hp} IsVisible {IsVisible}";
}
