﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

public class ItemEditorWindow : EditorWindow
{
    string myString = "Hello World";
    bool groupEnabled;
    bool myBool = true;
    float myFloat = 1.23f;
    [MenuItem("Window/Serializer")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        ItemEditorWindow window = (ItemEditorWindow)EditorWindow.GetWindow(typeof(ItemEditorWindow));
        window.Show();
    }
    async void OnGUI()
    {
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        myString = EditorGUILayout.TextField("Text Field", myString);
        EditorGUILayout.Vector3Field("Vector3", Vector3.one);
        groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
        EditorGUILayout.Vector4Field("Vector4", Vector4.one);
        myBool = EditorGUILayout.Toggle("Toggle", myBool);
        myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
        EditorGUILayout.EndToggleGroup();
        if (GUILayout.Button("Переименовать все объекты"))
        {
            await AccessTheWebAsync();
            return;
        }

        if (GUILayout.Button("Удалить все объекты"))
        {
            var objects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
            for (int i = 0; i < objects.Length; i++)
            {
                DestroyImmediate(objects[i]);
            }
        }
    }
    async Task AccessTheWebAsync()
    {
        List<string> names = new List<string>();
        var objects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();

        // You need to add a reference to System.Net.Http to declare client.  
        HttpClient client = new HttpClient();

        // GetStringAsync returns a Task<string>. That means that when you await the  
        // task you'll get a string (urlContents).  
        Task<string> getStringTask = client.GetStringAsync("https://www.seventhsanctum.com/generate.php?Genname=fantasynameex");


        string urlContents = await getStringTask;
        MatchCollection matches = Regex.Matches(urlContents, "<div class=\"(GeneratorResultSecondaryBG|GeneratorResultPrimeBG)\">(?<userName>.*?)</div>");
        foreach (Match match in matches)
        {
            var gr = match.Groups["userName"];
            names.Add(gr.ToString());
        }
        for (int i = 0; i < objects.Length; i++)
        {
            objects[i].name = names[i];
        }
    }
}
