﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WeaponUI))]
public class WeaponUIEditor : Editor
{
    // Start is called before the first frame update
    bool _isPressButtonOk;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        WeaponUI testTarget = (WeaponUI)target;

        var isPressButton = GUILayout.Button("Случайный цвет", EditorStyles.miniButton);

        if (isPressButton)
        {
            Random rnd = new Random();
            testTarget.Color = new Color(Random.value, Random.value, Random.value);
        }
        
    }
}
