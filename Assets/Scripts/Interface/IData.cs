﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IData
{
    void Save(Player player);
    Player Load();
    void SetOptions(string path);
}
