﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class StreamData : IData
{
    private string _path;

    public void Save(Player player)
    {
        using (var sw = new StreamWriter(_path))
        {
            sw.WriteLine(player.Name);
            sw.WriteLine(player.Hp);
            sw.WriteLine(player.IsVisible);
        }
    }

    public Player Load()
    {
        var reult = new Player();

        if (!File.Exists(_path)) return reult;
        using (StreamReader streamReader = new StreamReader(_path))
        {
            while (!streamReader.EndOfStream)
            {
                reult.Name = streamReader.ReadLine();
                reult.Hp = System.Convert.ToSingle(streamReader.ReadLine());
                reult.IsVisible = streamReader.ReadLine().TryBool();
            }
        }

        return reult;
    }

    public void SetOptions(string path)
    {
        _path = Path.Combine(path, "Data.GeekBrains.Stream");
    }
}

