﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

public class SaveLevel
{
    private static int _numberScene = 0;

    [MenuItem("Geekbrains/Сохранить уровень", false, 1)]
    private static void NewMenuOption()
    {
        string savePath = Path.Combine(Application.dataPath, "Data.xml");
        GameObject[] levelses = GameObject.FindObjectsOfType<GameObject>();
        List<SerializableGameObject> levelsList = new List<SerializableGameObject>();
        foreach (var o in levelses)
        {
            var trans = o.transform;
            levelsList.Add(new SerializableGameObject
            {
                Name = o.name,
                Pos = trans.position,
                Rot = trans.rotation,
                Scale = trans.localScale,
                Static = trans.gameObject.isStatic
            });
        }
        SerializableToXML.Save(levelsList.ToArray(), savePath);
    }
}