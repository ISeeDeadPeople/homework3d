﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class BinaryData : IData
{
    private string _path;
    BinaryFormatter formatter;
    public Player Load()
    {
        using (FileStream fs = new FileStream(_path, FileMode.OpenOrCreate))
        {
            return (Player)formatter.Deserialize(fs);
            
        }
    }

    public void Save(Player player)
    {
        formatter = new BinaryFormatter();
        // получаем поток, куда будем записывать сериализованный объект
        using (FileStream fs = new FileStream(_path, FileMode.OpenOrCreate))
        {
            formatter.Serialize(fs, player);
        }
    }

    public void SetOptions(string path)
    {
        _path = Path.Combine(path, "Data.GeekBrains.Binary") + ".dat";
    }
}
