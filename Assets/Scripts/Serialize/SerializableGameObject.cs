﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


[Serializable]
public struct SerializableGameObject
{
    public string Name;
    public SerializableVector3 Pos;
    public SerializableQuaternion Rot;
    public SerializableVector3 Scale;
    public bool Static;
}