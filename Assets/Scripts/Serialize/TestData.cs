﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestData : MonoBehaviour
{
    private DataManager _data = new DataManager();
    private void Start()
    {
        var path = Application.dataPath;
        _data.SetData<BinaryData>();
        _data?.SetOptions(path);

        var player = new Player
        {
            Name = "Roman",
            Hp = 1000,
            IsVisible = true
        };

        _data.SetData<BinaryData>();
        _data?.SetOptions(path);
        _data?.Save(player);

        _data.SetData<JsonData>();
        _data?.SetOptions(path);
        _data?.Save(player);

        _data.SetData<StreamData>();
        _data?.SetOptions(path);
        _data?.Save(player);

        _data.SetData<XMLData>();
        _data?.SetOptions(path);
        _data?.Save(player);

        var newPlayer = _data?.Load();

        Debug.Log(newPlayer);
    }
}
